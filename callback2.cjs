const fs = require('fs');

function readFile(listData, boardID, callback) {
    setTimeout(() => {
        fs.readFile(listData, 'utf8', (err, data) => {
            if (err) {
                callback(err);
            }
            else {
                let listParsedData = JSON.parse(data);
                //console.log(listParsedData[boardID]);
                callback(null, listParsedData[boardID]);
            }
        })
    }, 2000)
}

module.exports = readFile;
