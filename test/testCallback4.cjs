const callback4 = require('../callback4.cjs')

const requiredInfo = require('../callback1.cjs');
const requiredLists = require('../callback2.cjs');
const requiredCards = require('../callback3.cjs');

callback4(requiredInfo, requiredLists, requiredCards);