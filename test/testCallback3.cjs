const callback3 = require('../callback3.cjs');

const listID = 'qwsa221';

function callback(err, data) {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    }
}

callback3('cards.json', listID, callback);