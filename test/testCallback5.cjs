const callback5 = require('../callback5.cjs')

const requiredInfo = require('../callback1.cjs');
const requiredLists = require('../callback2.cjs');
const requiredCards = require('../callback3.cjs');

callback5(requiredInfo, requiredLists, requiredCards);