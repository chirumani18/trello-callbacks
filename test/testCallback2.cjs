const callback2 = require("../callback2.cjs");

const boardID = 'mcu453ed';

function callback(err, data) {
    if (err) {
        console.error(err);
    }
    else {
        console.log(data);
    }
}

callback2('lists.json', boardID, callback);