const fs = require('fs');

function mindListCards(requiredInfo, requiredLists, requiredCards) {
    requiredInfo('boards.json', 'mcu453ed', (err, reqBoard) => {
        if (err) {
            console.error(err);
        } else {
            console.log(reqBoard);


            let reqBoardId = reqBoard.id;
            requiredLists('lists.json', reqBoardId, (err, reqList) => {
                if (err) {
                    console.error(err);
                } else {
                    console.log(reqList);


                    let reqIdInfo = reqList.filter((object) => {
                        return object.name === 'Mind' || object.name === 'Space';
                    })
                    reqIdInfo.map((data) => {
                        let reqId = data.id;
                        requiredCards('cards.json', reqId, (err, reqCards) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log(reqCards);
                            }
                        });
                    })

                }
            });
        }
    });
}


module.exports = mindListCards;
