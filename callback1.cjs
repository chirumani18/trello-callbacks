const fs = require('fs');

function readFile(boardsData, boardID, callback) {
    setTimeout(() => {
        fs.readFile(boardsData, 'utf8', (err, data) => {
            if (err) {
                callback(err);
            }
            else {
                //data is in string form
                //console.log(data);

                //to convert to js objects
                let parsedData = JSON.parse(data);
                //console.log(parsedData);

                let reqIdInfo = parsedData.filter((obj) => {
                    return obj.id === boardID;
                })
                callback(null, reqIdInfo[0]);
            }
        })
    }, 2000)

}
module.exports = readFile;
