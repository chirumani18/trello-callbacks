const fs = require('fs');

function readFile(cardsData, listID, callback) {
    setTimeout(() => {
        fs.readFile(cardsData, 'utf8', (err, data) => {
            if (err) {
                callback(err);
            }
            else {
                let parsedData = JSON.parse(data);
                callback(null, parsedData[listID]);
            }
        })
    }, 2000)
}

module.exports = readFile;